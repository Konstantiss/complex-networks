import csv
import random
import networkx as nx
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import scipy.sparse as ss


def randomWalkNode(node, graph, numberOfSteps):
    cluster = set()
    for i in range(numberOfSteps):
        neighborhood = list(nx.edges(graph, node))
        if len(neighborhood) != 0:
            nextEdge = random.choice(neighborhood)
            node = nextEdge[1]
            cluster.add(node)
    return cluster

def randomWalkGraph(graph, numberOfSteps):
    clusters = dict()
    for node in nx.nodes(graph):
        clusters[node] = randomWalkNode(node, graph, numberOfSteps)
    return clusters


def convertToCoo(filename):
    "Read data file and return sparse matrix in coordinate format."
    data = pd.read_csv(filename, header=None, dtype=np.int64)
    rows = data[0]  # Not a copy, just a reference.
    cols = data[1]
    ones = np.ones(len(rows), np.int64)
    matrix = ss.coo_matrix((ones, (rows, cols)))
    return matrix

sparseMatrix = convertToCoo("soc-sign-bitcoinalpha.csv")
print(sparseMatrix)

graph = nx.from_scipy_sparse_matrix(sparseMatrix, parallel_edges=False, edge_attribute='weight')

print(nx.info(graph))

print(randomWalkGraph(graph, 40))
nx.write_gexf(graph, "test.gexf")



